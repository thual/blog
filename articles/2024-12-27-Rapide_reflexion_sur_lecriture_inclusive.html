<!doctype html>
<html lang="fr">

<head>
  <title>Rapide réflexion sur l'écriture inclusive</title>
  <!-- Favicon -->
  <link rel="shortcut icon" href="../assets/favicon.svg">
  <!-- CSS -->
  <link rel="stylesheet" href="../style.css">
  <!-- RSS -->
  <link rel="alternate" type="application/rss+xml" href="../feed.xml" title="RSS">
  <!-- Métadonnées -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="thual">
  <meta name="description" content="L'écriture inclusive, bien que relevant d'une excellente intention, pose plusieurs problèmes sur la langue. Voici donc quelques idées sur le sujet, ainsi qu'une proposition personnelle.">
  <!-- Dublin Core -->
  <link rel="schema.DCTERMS" href="http://purl.org/dc/terms/" />
  <meta name="DC.creator" content="thual" />
  <meta name="DC.title" content="Rapide réflexion sur l'écriture inclusive" />
  <meta name="DC.contributor" content="" />
  <meta name="DC.publisher" content="thual.eu" />
  <meta name="DC.created" content="2024-12-27" />
  <meta name="DC.identifier" content="2024-12-27-Rapide_reflexion_sur_lecriture_inclusive" />
  <meta name="DC.uri" content="https://thual.eu/articles/2024-12-27-Rapide_reflexion_sur_lecriture_inclusive.html" />
  <meta name="DC.license" content="https://creativecommons.org/licenses/by-nc-sa/4.0/" />
  <meta name="DC.language" content="fr" />
  <meta name="DC.modified" content="" />
  <meta name="DC.description" content="L'écriture inclusive, bien que relevant d'une excellente intention, pose plusieurs problèmes sur la langue. Voici donc quelques idées sur le sujet, ainsi qu'une proposition personnelle." />
  <meta name="DC.type" content="article" />
  <!-- Open Graph -->
  <meta property="og:title" content="Rapide réflexion sur l'écriture inclusive" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="https://thual.eu/articles/2024-12-27-Rapide_reflexion_sur_lecriture_inclusive.html" />
  <meta property="og:image" content="https://thual.eu/assets/og-image.png/" />
  <meta property="og:locale" content="fr" />
  <meta property="og:site_name" content="thual.eu" />
</head>

<body lang="fr">
<nav>
    <a href="../index.html">Accueil</a>
    <a href="../pages/blog.html">Blog</a>
    <a href="../pages/pages.html">Pages</a>
    <a href="../pages/a_propos.html">A propos</a>
  </nav>
  
  <h1 id="rapide-réflexion-sur-lécriture-inclusive">Rapide réflexion sur
l’écriture inclusive</h1>
<time datetime="2024-12-27">27 décembre 2024</time>
<h2 id="introduction">Introduction</h2>
<p>J’écris cette introduction après avoir écrit l’article, et je dois
avouer qu’il est plus long que ce que j’avais anticipé. Je me sens donc
forcé d’annoncer mon plan, vu la taille de l’article.</p>
<p>Je vais donc commencer par poser le cadre de la réflexion. Je
parlerai rapidement des raisons de l’écriture inclusive avant un petit
aparté sur la novlangue. Je continuerai sur les problèmes que je trouve
à l’écriture inclusive, avant de conclure sur une proposition
personnelle qui trotte dans mon cerveau depuis quelques années.</p>

<a href="#cadre-de-la-réflexion">Cadre de la réflexion</a>

<a href="#pourquoi-lécriture-inclusive">Pourquoi l'écriture inclusive ?</a>

<a href="#lécriture-inclusive-incarnation-de-la-novlangue">L'écriture inclusive, incarnation de la novlangue ?</a>

<a href="#les-soucis-apportés-par-lécriture-inclusive">Les soucis apportés par l'écriture inclusive</a>

<a href="#proposition-personnelle">Proposition personnelle</a>

<h2 id="cadre-de-la-réflexion">Cadre de la réflexion</h2>
<p>Je veux d’abord préciser que je n’ai fait que très peu de recherches
sur le sujet de l’écriture inclusive, et aucune de manière un tant soit
peu sérieuse.</p>
<p>Cependant, je pense en avoir suffisamment entendu parler pour avoir
une idée générale des débats.</p>
<p>Posons le cadre. Dans cet article, je considère l’écriture inclusive
comme un moyen de mettre à égalité femmes et hommes dans la langue
(française). Ce terme ne désigne donc pas spécifiquement certaines
techniques de l’écriture inclusive comme le point médian.</p>
<h2 id="pourquoi-lécriture-inclusive">Pourquoi l’écriture inclusive
?</h2>
<p>La langue française à l’heure actuelle place clairement le féminin à
un niveau inférieur au masculin. Ce n’est pas un hasard, c’est
volontaire et c’est même plutôt récent à l’échelle de la langue.</p>
<p>L’Académie française, en fixant les règles de la langue française, a
lutté pour supprimer un grand nombre de termes et de règles. Par
exemple, l’accord de proximité existait bien avant la règle de
supériorité du masculin dans l’usage courant. De même, le terme
“autrice” qui a causé beaucoup de débat il y a quelques années, existe
en réalité au moins depuis le premier siècle et a évolué depuis le
latin. Loin du néologisme évoqué par l’Académie, donc.</p>
<p>D’ailleurs, cette destruction des règles n’est pas forcément mauvaise
dans son principe. Fixer les règles d’une langue implique de faire un
choix dans ces règles, et donc nécessairement de déclarer “incorrectes”
certaines d’entre elles. Et avoir une langue unifiée aux règles bien
définies a autant d’avantages que d’inconvénients d’un point de vue
culturel.</p>
<p>Cependant, il faut avoir conscience que ce processus a été effectuée
par une Académie entièrement masculine, à une époque où la femme était
clairement considérée comme inférieure à l’homme. D’où une certaine
conception de la langue.</p>
<p>On peut se dire que la masculinité de la langue est un problème sans
grande importance, surtout en le comparant aux autres petits soucis qui
nous entourent. Cependant, il faut se rappeler que l’on pense dans sa
langue. La langue conditionne ce que l’on est capable de penser. Une
langue qui dans ses construction rabaisse systématiquement le féminin à
un niveau inférieur au masculin pose un problème majeur dans une société
qui se veut égalitaire.</p>
<h2 id="lécriture-inclusive-incarnation-de-la-novlangue">L’écriture
inclusive, incarnation de la novlangue ?</h2>
<p>J’en profite pour faire un petit aparté sur les opposants à
l’écriture inclusive qui parlent de “novlangue”, en référence au roman
1984 de Georges Orwell publié en 1948. Dans le roman, la novlangue
permet au pouvoir en place de mieux contrôler ce que pensent les gens.
Cette comparaison présente donc clairement l’écriture inclusive comme un
danger.</p>
<p>Cette comparaison est erronée. Je n’aime pas être aussi catégorique,
mais dans ce cas je ne peux vraiment pas concevoir cette
comparaison.</p>
<p>Orwell est clair, dès le début du livre. La novlangue est construite
par la destruction en masse des mots. La novlangue est conçue pour
réduire la capacité de réflexion des gens, et cela passe logiquement par
la destruction de mots par milliers et par la simplification massive des
règles de langage. C’est d’ailleurs un thème que l’on retrouve
régulièrement dans les romans de science-fiction.</p>
<p>En ayant ceci en tête, il est impossible de concevoir les néologisme,
au sens non péjoratif de “mots nouvellement créés”, ou la proposition de
nouvelles règles, comme de la novlangue ; c’est précisément
l’inverse.</p>
<p>Et je ne dis pas que l’écriture inclusive ne pose pas de problèmes,
j’y viens. Simplement, la comparaison avec la novlangue est un non sens
absolu.</p>
<h2 id="les-soucis-apportés-par-lécriture-inclusive">Les soucis apportés
par l’écriture inclusive</h2>
<p>Alors, quels sont les problèmes apportés par l’écriture inclusive ?
Sachez d’abord que je suis d’accord sur le fond, moins sur la forme.</p>
<p>Personnellement, je ne comprends pas les gens qui souhaitent
“protéger” la langue française. Une langue, par essence, évolue. Des
dizaines de mots sont ajoutés au dictionnaire tous les ans. Vous vous
imaginez discuter au quotidien dans le français de l’an 1 000 ?</p>
<p>Comme précédemment évoqué, la langue nous permet de penser, mais
c’est aussi et surtout elle qui borne ce que l’on peut penser. Elle est
le reflet de la société dans laquelle on vit. Si on veut faire évoluer
la société, il est obligatoire de faire évoluer la langue.</p>
<p>Et comme je souhaite sincèrement que notre société change vers le
meilleur, je souhaite aussi que la langue change pour accompagner
l’évolution de la société. Ou même pour l’aider, je ne sais pas vraiment
si la société influence la langue ou inversement. Probablement les deux,
en fait.</p>
<p>Ce qui me pose problème, c’est la perte de fluidité. La langue
française se base énormément sur la subtilité, sur les sous-entendus.
Cela permet de dire plus en parlant moins. Nous avons une langue qui
permet des niveaux de détails, de fluidité et de subtilité
extraordinaires. À ce niveau, je pense qu’une certaine conception de
l’écriture inclusive peut être dangereuse pour la langue française.</p>
<p>Je suis sincèrement convaincu de la nécessité de faire évoluer la
langue française, que ce soit pour la rendre plus inclusive ou pour
d’autres raisons. Cependant, je considère que la fluidité de la langue
doit toujours être la première considération lorsque l’on propose une
évolution.</p>
<p>Et soudainement, un grand nombre des outils de l’écriture inclusive
disparaissent. Le point médian qui hache les phrases, les concaténations
du type “auteurice” qui alourdissent les mots… Ces propositions sont
pour moi irrecevables, tout comme toute autre solution qui passe par une
perte de fluidité dans la langue.</p>
<p>En réalité, il existe un grand nombre de propositions pour l’écriture
inclusive, et je n’en connais que quelques-unes, probablement
les plus médiatisées. Je suis pratiquement certain qu’il en existe un
certain nombre que je ne connais pas mais qui me satisferaient.</p>
<h2 id="proposition-personnelle">Proposition personnelle</h2>
<p>Il y a une proposition qui me vient en tête assez
souvent. Elle est loin d’être parfaite, mais je l’aime bien et je
voudrais la soumettre au débat, d’autant qu’elle conclut une réflexion
assez précise (je crois).</p>
<p>Mon premier postulat, c’est que le plus simple serait d’avoir un
genre grammatical neutre, comme l’anglais et l’allemand. Cela
simplifierait probablement beaucoup les choses. Le problème, c’est que
la langue française n’est pas conçue pour. Rajouter un genre neutre
demanderait des modifications extrêmement importantes qui poseraient
beaucoup d’autres problèmes linguistiques et culturels, sans parler du
soutient énorme qu’elles nécessiteraient.</p>
<p>Partons du principe que cette modification n’aura pas lieu. Nous
devons donc composer avec deux genres seulement. La plupart des
propositions envisagées consiste à utiliser ces deux genres en même
temps lorsque nécessaire pour ne pas invisibiliser l’un des deux. Cette
manière de penser provoque forcément un alourdissement du langage, et n'est 
donc pas satisfaisantes.</p>
<p>Par conséquent, il faudrait pouvoir n’utiliser qu’un seul genre y
compris dans les situations qui exigent du neutre. C’est dans cet
objectif qu’existe la règle du masculin qui l’emporte toujours sur le
féminin. Dans ce cas, il existe plusieurs autres solutions plus
égalitaires comme l’accord de proximité, mais qui ne résolvent pas tous
les problèmes.</p>
<p>Un exemple :</p>
<ul>
<li><strong>Accord de supériorité du masculin :</strong> <em>Des hommes
et des femmes lettrés</em></li>
<li><strong>Accord de proximité :</strong> <em>Des hommes et des femmes
lettrées</em></li>
<li><strong>Cas limite :</strong> dira-t-on <em>Sont-ils lettrés</em> ou
<em>Sont-elles lettrées</em>, si on ne sait pas de qui on parle ?</li>
</ul>
<p>Comme on ne peut utiliser qu’un seul genre pour des raisons de
fluidité, il faut faire un choix dans les situations qui exigent du
neutre. Mais comment choisir ?</p>
<p>Alain Damasio propose, dans <em>La Vallée du Silicium</em>,
d’alterner les genres. Il précise, en page 2, que “Dans ce livre, les
pluriels neutres ont été féminisés une fois sur deux”. Il est cependant
clair sur le fait que ce n’est pas une solution satisfaisante.</p>
<p>Personnellement, je propose une manière plus humble, et à mes yeux
plus élégante, de faire ce choix. Dans les cas où il n’y a pas de
manière simple de choisir quel genre utiliser, alors on peut simplement
<strong>choisir le genre opposé au notre</strong> (masculin si on se
considère comme femme et inversement).</p>
<p>De toute manière, il s’agit d’un choix arbitraire. J’aime donc
beaucoup l’idée de choisir de représenter l’autre lorsque la grammaire
fait défaut. Je trouve que cela permet une certaine élégance qui colle à
l’idée que je me fais de la langue française.</p>
<p>Cela a aussi l’avantage d'autoriser une certaine habitude que ne
permet pas une solution basée sur l’alternance des genres comme proposée
par Damasio.</p>
<p>Évidemment, j’y vois immédiatement plusieurs soucis.</p>
<p>Le premier, c’est que la langue devient grammaticalement différente
pour les hommes et les femmes. Je ne sais pas vraiment si d’un point de
vue linguistique il s’agit d’une bonne idée. Cependant, je ne peux
m’empêcher de penser que la langue peut déjà être très différente entre
deux personnes, sans parler de genre.</p>
<p>Le plus gros problème à mes yeux concerne les personnes qui se
considèrent comme non binaires. Mais dans ce cas, je ne vois pas
vraiment de solution sans passer par un genre neutre dans la langue.</p>
<p>J’avais dit “rapide réflexion” ? Mon compteur indique déjà autour de
1500 mots. Je vais donc m’arrêter là et publier cet article.</p>
<p>J’aimerais beaucoup en discuter avec des personnes compétentes en
linguistique, ce que je ne suis pas du tout. En fait, j’apprécierais en
discuter avec vous ! N’hésitez pas à m’envoyer un mail pour en
parler.</p>

<footer>
  <hr>
  <a href="mailto:social@thual.eu">Répondre par email | social@thual.eu</a>
  <p>Thual</p>
</footer>

</body>
</html>