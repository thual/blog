# Blog
---

## Introduction

Ce dépôt contient le code source de mon blog personnel.

## Technologies utilisées

Ce site est codé en HTML et CSS uniquement, pour des raisons de simplicités et de légèreté.

## Lecture hors ligne

Pour lire le contenu du site hors ligne, un simple `git clone git@framagit.org:thual/blog.git` vous permet de télécharger entièrement le site sur votre machine. Vous n'avez plus qu'à ouvrir une des pages pour accéder au blog.

## Licence

Le code source de ce site est sous licence [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## Contact

Pour plus d'informations, vous pouvez me contacter à l'adresse mail `thual@labart-barnagaud.fr`, sur matrix à l'adresse `@thual:deuxfleurs.fr` ou sur Signal sous le nom de `thual.55`.