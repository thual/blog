#!/bin/bash

# Vérifie que le fichier Markdown est fourni
if [ $# -eq 0 ]; then
    echo "Usage: $0 <fichier_markdown>"
    exit 1
fi

confirmer() {
    read -p "Voulez-vous continuer ? (o/n) " choix
    if [[ "$choix" == "o" || "$choix" == "O" ]]; then
        echo "Le script continue."
    else
        echo "Opération annulée"
        rm $HTML_FILE temporaire.*
        exit 1
    fi
}

# Fichier Markdown en entrée
MD_FILE="$1"

# Fichiers
HEADER="templates/header.html"
NAV="templates/nav.html"
FOOTER="templates/footer.html"
XML_ITEM="templates/item.xml"
XML_FOOTER="templates/footer.xml"
XML_FILE="feed.xml"

# Extraire les métadonnées du document markdown
TITLE=$(grep "^title:" "$MD_FILE" | cut -d: -f2- | xargs)
CREATOR=$(grep "^creator:" "$MD_FILE" | cut -d: -f2- | xargs)
CONTRIBUTOR=$(grep "^contributor:" "$MD_FILE" | cut -d: -f2- | xargs)
CREATED=$(grep "^created:" "$MD_FILE" | cut -d: -f2- | xargs)
MODIFIED=$(grep "^modified:" "$MD_FILE" | cut -d: -f2- | xargs)
LICENSE=$(grep "^license:" "$MD_FILE" | cut -d: -f2- | xargs)
LANGUAGE=$(grep "^language:" "$MD_FILE" | cut -d: -f2- | xargs)
DESCRIPTION=$(grep "^description:" "$MD_FILE" | cut -d: -f2- | xargs)
TYPE=$(grep "^type:" "$MD_FILE" | cut -d: -f2- | xargs)
IDENTIFIER=$(grep "^identifier:" "$MD_FILE" | cut -d: -f2- | xargs)

# Métadonnées additionnelles
PUBLISHER="thual.eu"
URI="https://thual.eu/articles/$IDENTIFIER.html"
RSS_DATE=$(LANG=en_us_88591; date -d "$DATE 12:00:00" "+%a, %d %b %Y %H:%M:%S %Z")
READABLE_DATE=$(date -d "$DATE" +'%d %B %Y')

# Vérification des métadonnées
echo "VERIFICATION DES METADONNEES :"
echo "title: $TITLE"
echo "creator : $CREATOR"
echo "contributor: $CONTRIBUTOR"
echo "created: $CREATED"
echo "modified: $MODIFIED"
echo "license: $LICENSE"
echo "language: $LANGUAGE"
echo "description: $DESCRIPTION"
echo "type: $TYPE"
echo "identifier: $IDENTIFIER"
echo "URI: $URI"
echo "publisher: $PUBLISHER"
echo "date RSS: $RSS_DATE"
echo "date lisible: $READABLE_DATE"

confirmer

# Création du fichier HTML
echo "Création du fichier HTML"

HTML_FILE="$IDENTIFIER.html"

touch $HTML_FILE

# Ajout du header métadonnées à HTML_FILE
cat $HEADER >> $HTML_FILE

# Conversion des métadonnées dans le fichier HTML_FILE
sed -i "s|TITLE|$TITLE|g" $HTML_FILE
sed -i "s|CREATOR|$CREATOR|g" $HTML_FILE
sed -i "s|CONTRIBUTOR|$CONTRIBUTOR|g" $HTML_FILE
sed -i "s|CREATED|$CREATED|g" $HTML_FILE
sed -i "s|MODIFIED|$MODIFIED|g" $HTML_FILE
sed -i "s|LICENSE|$LICENSE|g" $HTML_FILE
sed -i "s|LANGUAGE|$LANGUAGE|g" $HTML_FILE
sed -i "s|DESCRIPTION|$DESCRIPTION|g" $HTML_FILE
sed -i "s|TYPE|$TYPE|g" $HTML_FILE
sed -i "s|IDENTIFIER|$IDENTIFIER|g" $HTML_FILE
sed -i "s|URI|$URI|g" $HTML_FILE
sed -i "s|PUBLISHER|$PUBLISHER|g" $HTML_FILE

# Ajout du menu de navigation fichier HTML_FILE
cat $NAV >> $HTML_FILE

# Création du fichier markdown temporaire sans les métadonnées
sed -e '1,/^---$/d' "$MD_FILE" > temporaire.md

# Conversion de temporaire.md vers temporaire.html
pandoc -f markdown -t html5 temporaire.md -o temporaire.html

# Ajout de la balise time dans le fichie temporaire
TIME="<time datetime=\"$CREATED\">$READABLE_DATE</time>"
sed -i "/<\/h1>/a\\$TIME" temporaire.html

# Ajout du corps de texte et du footer au fichier HTML_FILE
cat temporaire.html >> $HTML_FILE
cat $FOOTER >> $HTML_FILE

echo "Le fichier HTML suivant a été produit :"
echo -e "\n"
cat $HTML_FILE
echo -e "\n"

confirmer

# Création de l'entrée RSS

echo "Création de l'entrée RSS"

cp $XML_ITEM temporaire.xml

# Remplacement des métadonnées
sed -i "s|TITLE|$TITLE|g" temporaire.xml
sed -i "s|URI|$URI|g" temporaire.xml
sed -i "s|IDENTIFIER|$IDENTIFIER|g" temporaire.xml
sed -i "s|DESCRIPTION|$DESCRIPTION|g" temporaire.xml
sed -i "s|RSS_DATE|$RSS_DATE|g" temporaire.xml

echo "L'entrée RSS suivante a été produite :"
echo -e "\n"
cat temporaire.xml

echo -e "\nLe script va maintenant publier le fichier HTML ainsi que l'entrée RSS dans le répertoire principal. Veuillez vous assurer de la qualité des fichiers générés avant de continuer."

confirmer

# Ajout de l'entrée XML
sed -i "/<\/image>/r temporaire.xml" $XML_FILE

# Ajout du fichier HTML_FILE dans le dossier des articles
mv $HTML_FILE articles/

# Édition du fichier blog pour ajouter le nouvel article

ENTREE="  <a href=\"../articles/$HTML_FILE\">$READABLE_DATE - $TITLE</a>"

sed -i "/<h2>2024<\/h2>/a\\$ENTREE" pages/blog.html

echo "Publication terminé."

echo "Nettoyage des fichiers temporaires :"

rm temporaire.html temporaire.md temporaire.xml

echo "Fin de l'opération. L'article est publié sur le blog."
