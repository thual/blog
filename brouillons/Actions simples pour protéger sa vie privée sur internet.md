---
title: "Actions simples pour protéger sa vie privée sur internet"
creator: thual
contributor: 
created: 2025-01-21
modified: 
license: https://creativecommons.org/licenses/by-nc-sa/4.0/
language: fr
description: "Quelques actions et modes de pensée pour protéger sa vie privée sur internet."
type: article
identifier: 2025-01-21-Actions_simples_pour_proteger_sa_vie_privee_sur_internet
---
# Actions simples pour protéger sa vie privée sur internet

À l'heure du tout numérique, la vie privée sur internet sont devenus des enjeux majeurs. 

Pour avoir plus d'informations sur la vie privée et l'informatique, je recommande l'excellent article "Vie privée et informatique" sur Wikipédia :

[Wikipedia - Vie privée et informatique](https://fr.wikipedia.org/wiki/Vie_priv%C3%A9e_et_informatique)

Jetez un œil à la section "nuisances potentielles", c'est succinct mais intéressant.

Pour une ressource très complète et précise sur la vie privée et la sécurité numérique en général :

[privacyguides.org](https://www.privacyguides.org/fr/)

Si vous voulez avoir un résumé de ce que je proposé, rendez-vous directement dans la section "Résumé".

## Quelques conseils

D'abord, les informations les plus protégées sont celles que vous ne divulguez pas du tout. **Limitez la création de comptes en ligne** : à chaque fois que vous créez un compte, demandez vous si vous en avez vraiment besoin et si vous en pouvez pas effectuer la démarche sans compte. **Gardez une trace** de vos comptes, cela vous permet de savoir plus ou moins qui possède vos données.

Ensuite, lorsque vous choisissez une plateforme, **regardez son _business model_**. Si elle n'est pas financée par des dons mais qu'elle est quand même gratuite, alors elle ne respecte probablement pas votre vie privée. Le numérique n'est pas gratuit, loin de là, et quelqu'un doit forcément assumer les coûts.

Enfin, lorsque c'est possible, préférez le **chiffrement de bout en bout**. Cela signifie que même le service que vous utilisez ne peut pas lire les données qui sont stockées sur leurs serveurs, c'est donc très positif en terme de vie privée. Difficile de vendre des données qu'on ne possède pas. Cependant, le chiffrement de bout en bout n'est applicable que pour certains services où la plateforme n'a pas besoin de connaître les données.

## Outils proposés

Je propose ici quelques outils plus respectueux de la vie privée que ceux couramment utilisés. Je ne connais pas tous les outils qui existent, et cette liste est donc amenée à évoluer. Faites vos recherches !

### Messageries instantanées

**À éviter :**

- Telegram
- Snapchat
- Toutes les messageries intégrées des réseaux sociaux, telles que Instagram Directs
- Les SMS (n'importe quelle personne entre vous et l'antenne relais peut lire vos SMS)

**Moindre mal**

- WhatsApp : selon Meta, l'application utilise un protocole de chiffrement de bout en bout, mais Meta est célèbre pour son manque de respect et la manipulation manifeste de ses utilisateurs.

**Conseillé**

- Signal : l'application créée par un des co-créateurs de WhatsApp est soutenue par une fondation à but non lucratif, financée par les dons de ses utilisateurs. Elle a largement démocratisé le chiffrement de bout en bout et elle compte beaucoup d'utilisateurs.

### Webmails

**À éviter :**

- Les services mails des FAI : Orange mail, Free Mail, SFR mail...
- Les services mail des big tech : Outlook, Gmail, Yahoo...

**Conseillés :**

- Tuta Mail
- Proton Mail : suite complète comportant un agenda, un drive, un VPN, un gestionnaire de mots de passe...

### Drive

**À éviter :**

- Google drive
- Dropbox
- Microsoft 365

**Conseillé :**

- Proton Drive

### Moteurs de recherche

**À éviter :**

- Google Search
- Bing Search
- Ecosia

**Conseillés :**

- Startpage : mêmes résultats que Google mais complètement anonymisés donc sans tracking
- Kagi : moteur de recherche indépendant possédant son propre index

### Navigateurs Web

**À éviter :**

- Google Chrome
- Microsoft Edge
- Opéra
- Chromium et dérivés : Google utilise Chromium pour imposer de nouvelles normes dangereuses pour la vie privée

**Moindre mal :**

- Brave : si vraiment vous devez utiliser une base chromium

**Conseillé :**

- Firefox : ce navigateur est aujourd'hui très rapide et est supporté par la fondation Mozilla. Si vous voulez bidouiller, il est possible de facilement améliorer le navigateur avec des systèmes comme Betterfox ou Lifrewolf. 

## Parlons d'Apple

Apple fait énormément de publicité pour se présenter comme un écosystème sécurisé qui respecte votre vie privée.

Passons sur la sécurité, qui est un autre sujet (sachez en tous cas que c'est loin d'être aussi clair). Pour la vie privée, j'ai eu du mal à trouver des sources précises sur le sujet. Je pense personnellement qu'Apple limite les fuites de vos données personnelles à des tiers, sans pour autant les empêcher, pour des raisons économiques.

Apple vend des appareils, dont un argument de vente est la vie privée. Il est risqué pour Apple de ne pas respecter cette promesse, contrairement à un Google qui fait 90 % de son chiffre d'affaire en vendant de la publicité. 

Cependant on sait qu'Apple partage un identifiant publicitaire avec les applications installées, identifiant anonyme que Facebook est pourtant capable de relier à vous.

Je pense qu'utiliser les services Apple peut être un moyen facile d'améliorer un peu sa vie privée, ou au moins de limiter la casse. Cependant rentrer dans l'écosystème Apple a plusieurs autres inconvénients : peu de personnalisation, fonctionnalités limitées par Apple, contrôle total de tous vos services et appareils par Apple... et puis tout est hors de prix.

À titre personnel, je déconseillerais donc l'utilisation d'Apple dans un contexte de protection de la vie privée. Je pense aussi personnellement que nous devrions avoir un contrôle et une compréhension des appareils et logiciels que nous utilisons, ce qui va totalement à l'encontre de la philosophie d'Apple.

## Résumé

- Installez **Signal**, une alternative à WhatsApp, conçus par le créateur de Whatsapp, qui utilise le chiffrement de bout en bout et qui est déjà utilisée par beaucoup de personnes
- Utilisez **Firefox**
- Remplacez **Google Search** par **Kagi Search** si vous en avez les moyens ou **Startpage** sinon
- Utilisez **ProtonMail** ou **Tuta Mail** pour votre boite mail. Fuyez les boites mail des FAI ou des big tech comme Gmail.
- Utilisez **Proton Drive** ou un service comme **Nextcloud** et fuyez les Google Drive ou Dropbox
- Si vous le pouvez, installez une ROM dégooglisée comme **/e/ OS** (très bon compromis et très pratique) ou **Garphene OS**, l'idéal la vie privée et la sécurité

## Conclusion

Sachez qu'utiliser ces technologies permet de réduire le pouvoir des entreprises qui nous espionnent en permanence, mais aussi de faciliter l'accès et d'améliorer la vie privée de vos proches autant que la vôtre.

Par exemple, si un proche utilise ProtonMail mais que vous utilisez Gmail, alors Google connait quand même l'entièreté de votre conversation.

Protégez votre vie privée et celle de vos proches. Ces quelques actions ne sont pas suffisantes, mais elles limitent déjà la fuite de vos données personnelles.

Si vous avez le temps de le faire, renseignez vous, réfléchissez à vos usages, regardez qui peut accéder à vos données et comment, à quoi elles servent, qui en profite, quels sont les risques pour vous et pour les autres... Souvenez vous qu'aujourd'hui une bonne partie de votre vie personnelles, sociale, professionnelle ou administrative se passe sur internet !
